<?php

/**
 * Created by PhpStorm.
 * User: mathias
 * Date: 12/01/16
 * Time: 10:59
 */

namespace limaga\model;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{

    protected $table = 'client';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function famille() {
        return $this->hasMany('\limaga\model\Personne', 'id');
    }

    public function commande () {
        return $this->hasMany('\limaga\model\Panier', 'idPanier');
    }

    //relation Produit

    //relation Facture
}