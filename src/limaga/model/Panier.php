<?php
/**
 * Created by PhpStorm.
 * User: mathias
 * Date: 12/01/16
 * Time: 14:19
 */

namespace limaga\model;

use \Illuminate\Database\Eloquent\Model;


class Panier extends Model
{
    protected $table = 'panier';
    protected $primaryKey = 'idPanier';
    public $timestamps = false;

    //relation Facture

    //relation Client

    //relation Produit
}