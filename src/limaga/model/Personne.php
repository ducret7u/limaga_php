<?php
/**
 * Created by PhpStorm.
 * User: mathias
 * Date: 12/01/16
 * Time: 11:14
 */

namespace limaga\model;

use Illuminate\Database\Eloquent\Model;


class Personne extends Model
{
    protected $table = 'personne';
    protected $primaryKey = 'idPers';
    public $timestamps = false;

    public function clientLie () {
        return $this->belongsTo('\limaga\model\Client', 'id');
    }

    //FINI


}