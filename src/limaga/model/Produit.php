<?php
/**
 * Created by PhpStorm.
 * User: mathias
 * Date: 15/01/16
 * Time: 08:21
 */

namespace limaga\model;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $table = 'produit';
    protected $primaryKey = 'idProduit';
    public $timestamps = false;

    //relation Panier
}