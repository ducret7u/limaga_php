<?php
/**
 * Created by PhpStorm.
 * User: Justin Noel
 * Date: 08/01/16
 * Time: 09:47
 */

namespace limaga\vue;

use limaga\model\Produit;
use limaga\utils\Authentication;

class VueLimaga {

    private $l;

    public function __construct($tab = array()) {
        $this->l=$tab;
    }

    public function render($i) {
        switch($i) {
            case 1 : {
                $content = $this->acceuil();
                break;
            }

            case 2 : {
                $content = $this->connexion();
                break;
            }

            case 3 : {
                $content = $this->inscription();
                break;
            }

            case 4 : {
                $content = $this->inscriptionOk();
                break;
            }

            case 5 : {
                $content = $this->inscriptionErreur();
                break;
            }

            case 6 : {
                $content = $this->boutique();
                break;
            }

            case 7 : {
                $content = $this->panier();
                break;
            }
            case 8 : {
                $content = $this->connexionOk();
                break;
            }
            case 9 : {
                $content = $this->connexionErreur();
                break;
            }
            case 10 : {
                $content = $this->connecteDeja();
                break;
            }
            case 11 : {
                $content = $this->demanderValidation();
                break;
            }
            case 12 : {
                $content = $this->panierValide();
                break;
            }
        }
        if (Authentication::getAccessLvL() == 0) {
            $memberZone = <<<END
            <a href="/limaga_php/index.php/login">CONNEXION</a>
END;
        }
        if (Authentication::getAccessLvL() == 1) {
            $memberZone = <<<END
            <a href="/limaga_php/index.php/panier">Panier</a><br />
            <a href="/limaga_php/index.php/logout">Deconnexion</a></p>

END;
        }


        $html = <<<END
        <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" type="text/css" href="../style.css">
            <link rel="stylesheet" type="text/css" href="style.css">
            <title>Piscine de Limaga</title>
        </head>
        <body>
            <div id = "header">
                <h1>PISCINE DE LIMAGA</h1>
                <div id = "account">
                    <p>$memberZone</p>
                </div>
            </div>
            <div id = "nav">
                <ul id = "navigation">
                    <li><a href = "/limaga_php/index.php/">Informations Générales</a></li>
                    <li><a href = "/limaga_php/index.php/">Horraires d'Ouverture</a></li>
                    <li><a href = "/limaga_php/index.php/shop">Boutique</a></li>
                    <li><a href = "/limaga_php/index.php/">Contact</a></li>
                </ul>
            </div>

            <div id = "content">
                $content
            </div>
        </body>
    </html>
END;
        echo $html;
    }

    private function acceuil() {
        $html2 = <<<END
        <h1>Bienvenue</h1>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eu lectus eleifend, aliquam purus et, placerat lectus. Sed vitae pellentesque nibh. Integer a enim vestibulum turpis iaculis vestibulum. Aenean mollis, eros ac varius pretium, tellus purus bibendum turpis, eget aliquam nisl quam vel nunc. Donec eleifend tortor aliquam, lacinia ligula et, ornare urna. Sed et purus condimentum, pellentesque felis ac, tempus mauris. Quisque euismod leo id est sagittis aliquam ut non diam. Mauris efficitur sapien dui, et porta arcu commodo ac. Sed vulputate mi bibendum, luctus mauris condimentum, faucibus quam. Morbi finibus mi nisi, ut consectetur enim aliquet a.</p>
        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum malesuada, leo id vestibulum malesuada, ipsum ante commodo diam, id hendrerit urna odio sit amet ex. Sed semper nunc in sem mattis, consectetur tincidunt mi eleifend. Sed elementum ante non elit hendrerit, nec rhoncus dui tristique. Aliquam cursus nisl sed ante tincidunt, vel malesuada justo volutpat. Sed vehicula ex eros, nec dignissim ipsum dignissim ut. Quisque vehicula orci laoreet nisi commodo finibus. In hac habitasse platea dictumst. Vestibulum eleifend iaculis iaculis. Nullam maximus pretium urna vel congue. Vestibulum volutpat nunc est, eget bibendum justo sodales a. Curabitur vel tincidunt ipsum. Duis pharetra sed enim vitae pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis elit at odio consectetur malesuada lobortis id magna.</p>
        <p>Etiam aliquet dui sed dolor tincidunt eleifend. Sed tempor et eros et tempus. Nunc nulla nunc, malesuada eget neque quis, semper commodo risus. Duis vel lobortis leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas gravida ligula sed velit imperdiet, in aliquam leo ornare. Fusce eget urna tortor. Nam consectetur quam id ornare bibendum. Donec gravida, nisl a pellentesque pharetra, erat erat egestas metus, nec iaculis velit urna eget metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>

END;
        return $html2;
    }


    private function connexion() {
        $html2 = <<<END
        <form method="post" action="login">
            <p><label>Identifiant</label> : <input type="text" name="identifiant" /></p>
            <p><label>Mot de Passe</label> : <input type="password" name="mdp" /></p>
            <p><input type="submit" value="Valider" name="validateC"></code><p>
            <p> Si vous n'avez pas de compte Limaga </p>
            <p><a href = "/limaga_php/index.php/index.php">Cliquez ici</a></p>
        </form>
END;
        return $html2;
    }


    private function inscription() {
        $html2 = <<<END
        <form method="post" action="/limaga_php/index.php/register">
            <h2>Inscription</h2>
            <p><label>Adresse e-mail</label> : <input type="email" name="mail" /></p>
            <p><label>Identifiant : </label> : <input type="text" name="identifiant" /></p>
            <p><label>Mot de Passe</label> : <input type="password" name="mdp" /></p>
            <p><label>Nom</label> : <input type="text" name="nom" /></p>
            <p><label>Prenom</label> : <input type="text" name="prenom" /></p>
            <p><label>Date de naissance</label> : <input type="date" name="ddn"></code></p>
            <p><label>Adresse</label> : <input type="text" name="adresse" /></p>
            <p><label>Code Postal</label> : <input type="text" name="cp" /></p>
            <p><label>Ville</label> : <input type="text" name="ville" /></p>
            <p><label>Telephone</label> : <input type="tel" name="tel" /></p>
            <p><label>Niveau de Natation</label> : <select name="niveau"><option value = "1">1</option>
                                                           <option value = "2">2</option>
                                                           <option value = "3">3</option>
                                                   </select>
            </p>
            <p><input type="submit" value="Valider" name="validate"></code></p>
</form>
END;
        return $html2;
    }

    private function inscriptionOk() {
        $html2 = <<<END
        <h2>Message</h2>
        <p>Inscription effectuée</p>
        <p><a href="/limaga_php/index.php">Retour à la page principale</a></p>
END;
        return $html2;
    }

    private function inscriptionErreur() {
        $html2 = <<<END
        <h2>Erreur</h2>
        <p>Inscription non effectuée : erreur</p>
        <p><a href="/limaga_php/index.php/register">Retour à la page d'inscription</a></p>
END;
        return $html2;
    }

    private function connexionOk() {
        $html2 = <<<END
        <h2>Message</h2>
        <p>Connexion OK</p>
        <p><a href="/limaga_php/index.php">Retour à la page principale</a></p>
END;
        return $html2;
    }

    private function connexionErreur() {
        $html2 = <<<END
        <h2>Erreur</h2>
        <p>Connexion non effectuée : erreur</p>
        <p><a href="/limaga_php/index.php/login">Retour à la page de connexion</a></p>
END;
        return $html2;
    }

    private function connecteDeja() {
        $html2 = <<<END
        <h2>Erreur</h2>
        <p>Erreur : Vos droits ne vous permettent pas d'effectuer cette action</p>
        <p><a href="/limaga_php/index.php">Retour à la page principale</a></p>
END;
        return $html2;
    }


    private function boutique() {
        $shop = '';
        foreach ($this->l as $value) {
            $libelle = $value->libelle;
            $prix = $value->prix;
            $id = $value->idProduit;
            $shop = $shop. <<<END
                <h2>Boutique</h2>
                <tr>
                    <form method="post" action="/limaga_php/index.php/shop">
                        <td>$libelle</td>
                        <td>$prix €</td>
                        <td><button type="submit" name="idProduit" value="$id">+</button></td>
                    </form>
                </tr>

END;

        }
        $html2 = <<<END
            <table>
                $shop
            </table>
END;
        return $html2;
    }


    public function panier() {
        $prixTotal = 0;
        $panier = '';
        foreach ($this->l[0] as $key=>$value) {
            $prod = Produit::find($key);
            $libelle = $prod['libelle'];
            $prix = $prod['prix'];
            $qte = $this->l[1][$key];
            $total = $prix * $qte;
            $prixTotal = $prixTotal + $total;
            $panier = $panier. <<<END
            <tr>
                <td>$libelle</td>
                <td>$qte</td>
                <td>$prix €</td>
                <td>$total €</td>
            </tr>
END;
        }
        $html2 = <<<END
        <h2>Votre Panier</h2>
        <table>
            <tr>
                <th>Article</th>
                <th>Quantité</th>
                <th>Prix [€]</th>
                <th>TOTAL[€]</th>
            </tr>
            $panier
            <tr>
                <td></td>
                <td></td>
                <td>TOTAL Panier</td>
                <td>$prixTotal €</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <form method="post" action="/limaga_php/index.php/panier"><td><button type="submit" name="prixTotal" value="$prixTotal">Valider Panier</button></td></form>
            </tr>
        </table>
END;
        return $html2;
    }

    private function demanderValidation() {
        $total = $this->l[0];
        $html2 = <<<END
            <p>Voulez vous valider votre panier ? (prix total :$total €)</p>
            <form method="post" action="/limaga_php/index.php/validatePanier">
                <button type="submit" name="prixTotal" value="$total">Oui</button>
            </form>
            <form method="get" action="/limaga_php/index.php/panier">
                <button type="submit" name="prixTotal" value="$total">Non</button>
            </form>
END;
        return $html2;
    }

    private function panierValide() {
        $html2 = <<<END
            <p>Panier validé, vous pouvez continuer votre visite</p>
            <p><a href = "/limaga_php/index.php/">Retourner à l'index</a><p>
END;
        return $html2;

    }

}