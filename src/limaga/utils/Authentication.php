<?php
/**
 * Created by PhpStorm.
 * User: mathias
 * Date: 11/01/16
 * Time: 17:34
 */

namespace limaga\utils;


class Authentication
{
    /**
     * A la base, j'utilisais un password_hash, mais WebEtu étant encore à l'ère
     * du PHP 5.4, j'ai innové en utilisant une manière détournée de ce que j'ai
     * observé sur des moteurs de forum comme MyBB. Si un moteur si courant utilise
     * ce système, c'est que c'est pas trop mauvais non ?
     *
     * Je pensais aussi généré de façon aléatoire dans la base de donnée le salt pour
     * chaque utilisateur, mais cela implique des modifications dans tous les sens, et
     * nous n'avons pas eu le temps.
     *
     * (Une MaJ de PHP sur WebEtu serait sympatique !)
     */
    public static function securiserMdp ($password) {
        $salt = '([([19-*(giL=}';
        $salth = sha1($salt);
        $passfinal = $salth . $password . $salth;
        $hash = sha1($passfinal);
        return $hash;
    }


    /**
     * @return int :
     *              0 - utilisateur non connecté
     *              1 - client simple
     *              2 - administrateur
     * Cela permet à la fois de check les accès et d'afficher des parties uniquement
     * à des personnes connectées ou déconnectées.
     */
    public static function getAccessLvL() {
        if (isset($_SESSION['login']) && isset($_SESSION['aLvl'])) {
            $res = $_SESSION['aLvl'];
        } else {
            $res = 0;
        }
        return $res;
    }




}