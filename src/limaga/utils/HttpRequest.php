<?php
/**
 * Created by PhpStorm.
 * User: Mathias VINCENT
 * Date: 10/12/15
 * Time: 10:59
 */

namespace limaga\utils;

class HttpRequest {

    protected $method, $script_name, $path_info, $query, $get, $post;

    public function __construct() {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->script_name = $_SERVER['SCRIPT_NAME'];
        if (isset($_SERVER['PATH_INFO'])) {
            $this->path_info = $_SERVER['PATH_INFO'];
        }
        $this->query = $_SERVER['QUERY_STRING'];
        $this->get = $_GET;
        $this->post = $_POST;
    }

    public function __get($attname) {
        if (property_exists ($this, $attname)) {
            return $this->$attname ;
        }
        else throw new Exception ("$attname : invalid property") ;
    }

    public function __set($attname, $attrval) {
        if (property_exists($this, $attname)) {
            $this->$attname = $attrval ;
            return $this->$attname ;
        }
        else throw new Exception ("$attname : invalid property") ;
    }

}
