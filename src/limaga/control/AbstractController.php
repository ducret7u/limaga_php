<?php
/**
 * Created by PhpStorm.
 * User: Mathias VINCENT
 * Date: 10/12/15
 * Time: 11:25
 */

namespace limaga\control;

use limaga\utils\HttpRequest;

abstract class AbstractController {

    protected $requete;

    public function __construct(HttpRequest $request = null) {
        $this->requete = $request;
    }

    public function __get($attname) {
        if (property_exists ($this, $attname)) {
            return $this->$attname ;
        }
        else throw new Exception ("$attname : invalid property") ;
    }

    public function __set($attname, $attrval) {
        if (property_exists($this, $attname)) {
            $this->$attname = $attrval ;
            return $this->$attname ;
        }
        else throw new Exception ("$attname : invalid property") ;
    }
}