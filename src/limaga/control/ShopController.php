<?php
/**
 * Created by PhpStorm.
 * User: Justin Noel
 * Date: 12/01/16
 * Time: 10:35
 */

namespace limaga\control;

use limaga\model\Panier;
use limaga\model\Produit;
use limaga\vue\VueLimaga;
use limaga\utils\Authentication;
use Slim\Slim;
use limaga\utils\HttpRequest;

class ShopController extends AbstractController {

    public function __construct(HttpRequest $request = null) {
        parent::__construct($request);
    }

    public function showShop() {
        $lProduits = Produit::all();
        $v = new VueLimaga($lProduits);
        if (Authentication::getAccessLvL() != 0) {
            $v->render(6);
        } else {
            $v->render(10);
        }

    }

    public function addProduct() {
        $app = Slim::getInstance();
        $tabPost = $app->request->post();
        $tabCookies = $app->request->cookies;
        $p = Produit::find($tabPost['idProduit']);
        if (isset($tabCookies['panier_limaga'])) {
            $panier = unserialize($tabCookies['panier_limaga']);
            $qte = unserialize($tabCookies['qte_panier_limaga']);
            if(isset($panier[$p->idProduit])) {
                $qte[$p->idProduit] = $qte[$p->idProduit] + 1;
            } else {
                $panier[$p->idProduit] = $p;
                $qte[$p->idProduit] = 1;
            }
        } else {
            $panier = array($p->idProduit=>$p);
            $qte = array($p->idProduit=>1);
        }
        setcookie('panier_limaga', serialize($panier), time() + 60 * 60 * 24 * 30);
        setcookie('qte_panier_limaga', serialize($qte), time() + 60 * 60 * 24 * 30);
        $lProduits = Produit::all();
        $vue = new VueLimaga($lProduits);
        $vue->render(6);
    }

    public function showPanier() {
        $app = Slim::getInstance();
        $tabCookies = $app->request->cookies;
        if (isset($tabCookies['panier_limaga'])) {
            $panier = unserialize($tabCookies['panier_limaga']);
            $qte = unserialize($tabCookies['qte_panier_limaga']);
            $tab[0] = $panier;
            $tab[1] = $qte;
            $vue = new VueLimaga($tab);
            if (Authentication::getAccessLvL() != 0) {
                $vue->render(7);
            } else {
                $vue->render(10);
            }
        }
    }

    public function panierDemandeValidation() {
        $app = Slim::getInstance();
        $tabPost = $app->request->post();
        $total = $tabPost['prixTotal'];
        $tab[] = $total;
        $vue = new VueLimaga($tab);
        if (Authentication::getAccessLvL() != 0) {
            $vue->render(11);
        }
        else {
            $vue->render(10);
        }
    }

    public function panierValide() {
        $app = Slim::getInstance();
        $tabPost = $app->request->post();
        $total = $tabPost['prixTotal'];
        $p = new Panier();
        $p->montant = $total;
        $p->id = $_SESSION['login'];
        $p->save();
        setcookie('panier_limaga', serialize(array()), time() + 60 * 60 * 24 * 30);
        setcookie('qte_panier_limaga', serialize(array()), time() + 60 * 60 * 24 * 30);
        $vue = new VueLimaga();
        if (Authentication::getAccessLvL() != 0) {
            $vue->render(12);
        } else {
            $vue->render(10);
        }

    }
}