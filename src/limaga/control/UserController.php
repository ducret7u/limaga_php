<?php
/**
 * Created by PhpStorm.
 * User: Justin Noel
 * Date: 08/01/16
 * Time: 09:36
 */

namespace limaga\control;

use DateTime;
use Illuminate\Support\Facades\Auth;
use limaga\model\Client;
use limaga\model\Personne;
use limaga\utils\Authentication;
use limaga\utils\HttpRequest;
use limaga\vue\VueLimaga;
use Slim\Slim;

class UserController extends AbstractController {

    public function __construct(HttpRequest $request = null) {
        parent::__construct($request);
    }

    public function setLogin() {
        $vue = new VueLimaga();
        $vue->render(2);

    }

    public function checkLogin()
    {
        $app = Slim::getInstance();
        $a = $app->request->post();
        $vue = new VueLimaga();
        if (!isset($_SESSION['login'])) {
            $login = $a['identifiant'];
            $mdp = $a['mdp'];
            $c = Client::find($login);

            if (isset($c) && isset($a['validateC']) && $a['validateC'] == 'Valider') {
                if (Authentication::securiserMdp($mdp) == $c->mdp) {
                    $_SESSION['login'] = $login;
                    $_SESSION['aLvl'] = $c->accessLvl;
                    $vue->render(8);
                } else {
                    $vue->render(9);
                }
            } else {
                $vue->render(9);
            }

        }
        else {
            $vue->render(10);
        }
    }

    public function setRegister() {
        $vue = new VueLimaga();
        echo Authentication::getAccessLvL();
        if (Authentication::getAccessLvL() != 0) {
            $vue->render(10);
        } else {
            $vue->render(3);
        }

    }

    public function saveRegister() {
        $c = new Client();
        $p = new Personne();
        $app = Slim::getInstance();
        $a = $app->request->post();
        $vue = new VueLimaga();
        if (isset($a['validate']) && $a['validate'] == 'Valider') {
            if (!filter_var($a['identifiant'])) {
                filter_var($a['identifiant'], FILTER_SANITIZE_STRING);
            }
            $c->id = $a['identifiant'];
            if (!filter_var($a['mail'])) {
                filter_var($a['mail'], FILTER_SANITIZE_EMAIL);
            }
            $c->email = $a['mail'];
            $mdp = Authentication::securiserMdp($a['mdp']);
            $c->mdp = $mdp;
            if (!filter_var($a['nom'])) {
                filter_var($a['nom'], FILTER_SANITIZE_STRING);
            }
            $p->nom = $a['nom'];
            if (!filter_var($a['prenom'])) {
                filter_var($a['prenom'], FILTER_SANITIZE_STRING);
            }
            $p->prenom = $a['prenom'];
            //revoir en dessous le filtre des dates
            $p->dateNaiss = $a['ddn'];
            if (!filter_var($a['adresse'])) {
                filter_var($a['adresse'], FILTER_SANITIZE_STRING);
            }
            $c->adresse = $a['adresse'];
            if (!filter_var($a['cp'])) {
                filter_var($a['cp'], FILTER_SANITIZE_STRING);
            }
            $c->codePostal = $a['cp'];
            if (!filter_var($a['ville'])) {
                filter_var($a['ville'], FILTER_SANITIZE_STRING);
            }
            $c->ville = $a['ville'];
            if (!filter_var($a['tel'])) {
                filter_var($a['tel'], FILTER_SANITIZE_STRING);
            }
            $c->numTel = $a['tel'];
            $p->niveauNat = $a['niveau'];
            $p->id = $c->id;
            $c->accessLvl = 1;
            $c->save();
            $p->save();
            $vue->render(4);
        }
        else {

            $vue->render(5);
        }
    }

    public function logout() {
        session_destroy();
        $vue = new VueLimaga();
        header("Refresh:0; url=/limaga_php/index.php/");
        $vue->render(1);
        exit;
    }

}