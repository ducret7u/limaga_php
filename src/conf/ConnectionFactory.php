<?php
/**
 * Created by PhpStorm.
 * User: Justin Noel
 * Date: 17/01/16
 * Time: 21:23
 */

namespace conf;

use Illuminate\Database\Capsule\Manager as DB;

class ConnectionFactory {

    private static $tabConfig;

    public static function setConfig( $file ) {
        self::$tabConfig = parse_ini_file($file);
    }

    public static function makeConnection() {
        $db = new DB();
        $db->addConnection(static::$tabConfig);
        $db->setAsGlobal();
        $db->bootEloquent();
        return $db;
    }

}