<?php
/**
 * Created by PhpStorm.
 * User: Justin Noel
 * Date: 08/01/16
 * Time: 09:10
 */
session_start();
require_once('vendor/autoload.php');

use limaga\vue\VueLimaga;
use limaga\control\UserController;
use limaga\utils\HttpRequest;
use limaga\control\ShopController;
use conf\ConnectionFactory;

ConnectionFactory::setConfig('db.etuapp.conf.ini');

$co = ConnectionFactory::makeConnection();

$req = new HttpRequest();

$app = new \Slim\Slim();

$app->get('/', function () {
    $v = new VueLimaga();
    $v->render(1);
});

$app->get('', function () {
    $v = new VueLimaga();
    $v->render(1);
});

$app->get('/login', function () {
    $c = new UserController();
    $c->setLogin();
});

$app->get('/register', function () {
    $c = new UserController();
    $c->setRegister();
});

$app->get('/shop', function () {
    $c = new ShopController();
    $c->showShop();
});

$app->post('/login', function () {
    $c = new UserController();
    $c->checkLogin();
});

$app->get('/logout', function () {
    $c = new UserController();
    $c->logout();
});

$app->post('/register', function () {
    $c = new UserController();
    $c->saveRegister();
});

$app->post('/shop', function () {
    $c = new ShopController();
    $c->addProduct();
});

$app->get('/panier', function () {
    $c = new ShopController();
    $c->showPanier();
});

$app->post('/panier', function () {
    $c = new ShopController();
    $c->panierDemandeValidation();
});

$app->post('/validatePanier', function () {
    $c = new ShopController();
    $c->panierValide();
});

$app->run();